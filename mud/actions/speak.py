from .action import Action3
from mud.events import SpeakEvent

class SpeakAction(Action3):
	EVENT = SpeakEvent
	ACTION = "speak"
	RESOLVE_OBJECT = "resolve_for_operate"
	RESOLVE_OBJECT2 = "resolve_for_operate"

	def resolve_object2(self):
		return self.object2
