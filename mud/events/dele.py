from .event import Event2

class DeleteEvent(Event2):
	NAME = "dele"
	
	def perform(self):
		cont = self.actor.container()
		self.object.move_to(cont)
		self.inform("dele")
