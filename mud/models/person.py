# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .model import Model
from .mixins.located import Located

class Person(Located, Model):

    """Une personne est dans un contenaire, soit un monde"""
    def __init__(self, **kargs):
        super().__init__(**kargs)

    def init_from_yaml(self, data, world):
        super().init_from_yaml(data, world)

    def update_from_yaml(self, data, world):
        super().update_from_yaml(data, world)

    def archive_into(self, obj):
        super().archive_into(obj)
